FROM adoptopenjdk/openjdk8
WORKDIR /usr/src/app
EXPOSE 8080
CMD [ "java","-jar","cars-api.jar" ]
COPY target/cars-api.jar cars-api.jar
