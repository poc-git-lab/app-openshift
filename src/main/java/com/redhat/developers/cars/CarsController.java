package com.redhat.developers.cars;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/cars")
public class CarsController {

	@RequestMapping(value = "/home",produces = "text/html")
    public String home() {
        return "<h1>Hello!!</h1><p><h2>Cars API</h2></p><p>Click <a href=\"/cars/list\">list</a> to get a " +
            "list of my favorite cars</p>";
    }
// simulando um commit no git para disparar um build e deploy 2.0
    @RequestMapping(value = "/list", produces = "application/json")
    public List<String> cars() {
        return Arrays.asList("BMW 1.0", "Hyundai Verna 2.0", "Audi 2.0", "Ferrari 2.0");
    }
    
    @RequestMapping(value = "/isAlive",produces = "text/html")
    public String isAlive() {
        return "ok";
    }
    
}
